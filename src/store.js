import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: 'vuex',
  storage: window.localStorage
});

const state = {
  logged: false
};

const getters = {};

const mutations = {
  login (state) {
    state.logged = true;
  },

  logout (state) {
    state.logged = false;
  }
};

const actions = {

};


const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  plugins: [vuexLocalStorage.plugin]
});

export default store;
